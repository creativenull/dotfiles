# Configs for my development environment

+ [SpaceVim](https://github.com/SpaceVim/SpaceVim)
+ [alacritty](https://github.com/alacritty/alacritty)
+ [efm language server](https://github.com/mattn/efm-langserver)
+ [emacs (Doom)](https://github.com/hlissner/doom-emacs)
+ [git](https://git-scm.com/)
+ [kitty](https://sw.kovidgoyal.net/kitty/)
+ [lsd](https://github.com/Peltoche/lsd)
+ [neovim](https://neovim.io)
+ [neovim (nightly - v0.5)](https://github.com/neovim/neovim)
+ [tmux](https://github.com/tmux/tmux)
+ [vscode](https://code.visualstudio.com/)
+ [zsh](https://www.zsh.org/)
