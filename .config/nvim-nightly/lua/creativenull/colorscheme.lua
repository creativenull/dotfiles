vim.opt.number = true
vim.opt.termguicolors = true
vim.cmd 'colorscheme zephyr'
require 'colorizer'.setup()
