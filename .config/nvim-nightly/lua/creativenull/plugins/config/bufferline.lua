vim.opt.termguicolors = true
require 'bufferline'.setup {
  options = {
    indicator_icon = '',
    close_icon = '',
    buffer_close_icon = '',
    show_buffer_close_icons = false,
    show_close_icon = false
  }
}
