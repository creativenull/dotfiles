vim.g.indent_blankline_char_list = { '▏', '▏', '▏', '▏', '▏' }
vim.g.indent_blankline_char_highlight_list = {
  'LspDiagnosticsSignInformation',
  'LspDiagnosticsSignHint',
  'LspDiagnosticsSignWarning',
  'LspDiagnosticsSignError'
}
vim.g.indent_blankline_space_char = ' '
vim.g.indent_blankline_space_char_blankline = ' '
vim.g.indent_blankline_show_first_indent_level = false
vim.g.indent_blankline_filetype_exclude = { 'help', 'packer', 'TelescopePrompt' }
